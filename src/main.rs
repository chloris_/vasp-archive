mod archival;
mod argument_parsing;
mod dir_analysis;

/// Program entry point.
fn main() {
    let args = argument_parsing::parse_arguments();

    if args.dirs_to_archive.is_empty() {
        // Archive subdirs of current working directory
        match archival::archive_subdirs_to_file(".", &args.output_archive) {
            Ok(()) => println!("Archival to '{}' succeeded.", args.output_archive),
            Err(e) => eprintln!("Error while archiving calculation subdirectories:\n{}", e),
        };
    } else {
        // Archive dirs passed as command line arguments
        match archival::archive_dir_list_to_file(&args.dirs_to_archive, &args.output_archive) {
            Ok(()) => println!("Archival to '{}' succeeded.", args.output_archive),
            Err(e) => eprintln!("Error while archiving specified directories:\n{}", e),
        };
    }
}
