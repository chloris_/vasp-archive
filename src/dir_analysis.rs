//! Functions for determination and analysis of VASP calculation directories.

use std::ffi::OsStr;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

//
// Constants
//

/// List of files that a directory must contain to be treated as a VASP
/// calculation directory. These files will also be archived.
pub const REQUIRED_FILES: [&'static str; 4] = ["POSCAR", "POTCAR", "KPOINTS", "INCAR"];

/// List of files from a VASP calculation directory, that will be archived in
/// addition to the ones in [REQUIRED_FILES], if they exist.
///
/// Files in this list support simple globbing (no subdirectories, only the
/// first part of the file name - `*` at the beginning of the name).
pub const OPTIONAL_FILES: [&'static str; 6] = [
    "OUTCAR",
    "CONTCAR",
    "XDATCAR",
    "vasprun.xml",
    "runvasp.sh",
    "*.txt",
];

//
// Functions
//

/// Checks if directory with provided `path` is a VASP calculation directory.
/// Returns [true] if it is and [false] otherwise.
///
/// To be treated as a VASP calculation directory, it must contain all files
/// in [REQUIRED_FILES].
pub fn is_calculation_dir<P: AsRef<Path>>(path: P) -> io::Result<bool> {
    let path = path.as_ref();

    let entries = fs::read_dir(path)?
        .map(|res| res.map(|e| e.path()))
        .collect::<Result<Vec<_>, io::Error>>()?;

    let entry_filenames: Vec<_> = entries.iter().filter_map(|e| e.file_name()).collect();

    #[cfg(test)]
    {
        println!("Dir '{}' entries:\n{:#?}", path.display(), entries);
        println!(
            "Dir '{}' filenames:\n{:#?}",
            path.display(),
            entry_filenames
                .iter()
                .map(|e| e.to_string_lossy())
                .collect::<Vec<_>>(),
        );
    }

    for req_file in REQUIRED_FILES {
        let os_req_file = OsStr::new(req_file);
        if !entry_filenames.contains(&os_req_file) {
            #[cfg(test)]
            println!("Missing required file '{}'", req_file);

            return Ok(false);
        }
    }

    return Ok(true);
}

/// Iterates provided directory `dir` and returns list of paths to first-level
/// subdirectories, that qualify as VASP calculation directories.
///
/// Subdirectories are checked using [is_calculation_dir]. The check is
/// non-recursive.
pub fn get_calculation_dirs<P: AsRef<Path>>(dir: P) -> io::Result<Vec<PathBuf>> {
    let dir = dir.as_ref();
    let mut calc_dirs: Vec<PathBuf> = Vec::new();

    for entry in fs::read_dir(dir)? {
        let entry_path = entry?.path();

        if !entry_path.is_dir() {
            #[cfg(test)]
            println!("Skipping a non-directory entry'{}'", entry_path.display());

            continue;
        }

        #[cfg(test)]
        println!("Testing entry '{}'", entry_path.display());

        if is_calculation_dir(&entry_path)? {
            #[cfg(test)]
            println!("Entry '{}' is calc dir", entry_path.display());

            calc_dirs.push(entry_path);
        }
    }

    return Ok(calc_dirs);
}

/// Returns a list of files to archive in VASP calculation directory `dir`.
/// Path are returned relatively to the directory `dir`.
///
/// The list of files is composed of file names in [REQUIRED_FILES] and any
/// existing files in [OPTIONAL_FILES].
pub fn get_files_to_archive<P: AsRef<Path>>(dir: P) -> io::Result<Vec<PathBuf>> {
    let dir = dir.as_ref();
    if !dir.is_dir() {
        return Err(io::Error::new(
            io::ErrorKind::NotFound,
            format!("'{}' is not a directory.", dir.display()),
        ));
    }

    let mut files: Vec<PathBuf> = Vec::new();

    // Closure for constructing file paths. Returns a tuple of two paths:
    // * The first path is the path to the file (relative to the current
    //   working directory or as passed).
    // * The second path is path to the file relative to its calculation
    //   directory.
    let construct_paths = |file_path: &str| -> (PathBuf, PathBuf) {
        let mut rel_path = PathBuf::new();
        rel_path.push(file_path);

        let mut path = PathBuf::new();
        path.push(&dir);
        path.push(&rel_path);

        return (path, rel_path);
    };

    for file in REQUIRED_FILES {
        let (path, rel_path) = construct_paths(file);

        if path.is_file() {
            files.push(rel_path);
        } else {
            // Required file not found
            return Err(io::Error::new(
                io::ErrorKind::Other,
                format!(
                    "Required file '{}' not found in directory '{}'.",
                    file,
                    dir.display(),
                ),
            ));
        }
    }

    for file in OPTIONAL_FILES {
        if file.starts_with('*') {
            // Resolve files with simple globbing

            // Only globs beginning with a single `*` are supported
            let n_matches = file.matches('*').count();
            if n_matches > 1 {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    format!(
                        "Only globs with a single '*' at the beginning of \
                        the file name are supported ({} wildcards found)",
                        n_matches,
                    ),
                ));
            }

            // Find all files in the directory with matching suffix
            let suffix = file.strip_prefix('*').unwrap();
            #[cfg(test)]
            println!(
                "Matching file name '{}' to files with suffix '{}'.",
                file, suffix
            );

            for entry in fs::read_dir(&dir)? {
                let entry = entry?;
                let file_name = entry.file_name().to_string_lossy().into_owned();

                if file_name.ends_with(suffix) && entry.file_type()?.is_file() {
                    #[cfg(test)]
                    println!("Entry '{}' matches suffix '{}'.", file_name, suffix,);

                    let mut rel_path = PathBuf::new();
                    rel_path.push(entry.file_name());
                    files.push(rel_path);
                }
            }
        } else {
            // In case of a normal file name resolve the file ordinarily
            let (path, rel_path) = construct_paths(file);

            if path.is_file() {
                files.push(rel_path);
            }
        }
    }

    return Ok(files);
}

//
// Unit tests
//

#[cfg(test)]
mod tests {
    use super::*;

    /// Path to the directory with test data.
    const TEST_DATA_PATH: &'static str = "test_data";

    /// Test directories that should be recognized as valid VASP calculation
    /// directories.
    const CALC_DIRS: [&'static str; 2] = ["test_data/calc_dir_1", "test_data/calc_dir_2"];

    /// Test directories that should not be recognized as valid VASP
    /// calculation directories.
    const NON_CALC_DIRS: [&'static str; 2] = ["test_data/non_calc_dir_1", "test_data/empty_dir"];

    #[test]
    fn test_is_calculation_dir() {
        for dir in CALC_DIRS {
            println!(":: Testing calc dir '{}'", dir);
            let result = is_calculation_dir(dir).unwrap();
            assert_eq!(result, true);
        }

        for dir in NON_CALC_DIRS {
            println!(":: Testing non-calc dir '{}'", dir);
            let result = is_calculation_dir(dir).unwrap();
            assert_eq!(result, false);
        }
    }

    #[test]
    fn test_get_calculation_dirs() {
        let result = get_calculation_dirs(TEST_DATA_PATH).unwrap();

        // Create a mutable collection of expected results in `PathBuf` form,
        // since the results are also in this form
        let mut exp_entries: Vec<PathBuf> = Vec::new();
        for path in CALC_DIRS {
            let mut buf = PathBuf::new();
            buf.push(path);

            exp_entries.push(buf);
        }

        // Check that each result entry has a corresponding entry in expected
        // entries, while removing found entries from expected entries
        for dir in result.iter() {
            match exp_entries.iter().position(|e| e == dir) {
                Some(i) => exp_entries.remove(i),
                None => {
                    panic!(
                        "Entry in results '{}̈́' not found in expected entries",
                        dir.display(),
                    );
                }
            };
        }

        // All expected entries should be accounted for
        assert!(
            exp_entries.is_empty(),
            "Not all expected entries were accounted for:\n{:#?}\n",
            exp_entries,
        );
    }

    #[test]
    fn test_get_files_to_archive() {
        let test_cases = [
            (
                "test_data/calc_dir_1",
                vec!["INCAR", "KPOINTS", "POSCAR", "POTCAR"],
            ),
            (
                "test_data/calc_dir_2",
                vec![
                    "comment.txt",
                    "INCAR",
                    "KPOINTS",
                    "OUTCAR",
                    "POSCAR",
                    "POTCAR",
                    "vasprun.xml",
                    "XDATCAR",
                ],
            ),
        ];

        for (dir_path, exp_files_str) in test_cases {
            println!(
                ":: Getting list of files to archive from directory '{}'",
                dir_path,
            );

            let result = get_files_to_archive(dir_path).unwrap();

            // Build a vector of expected file path in `PathBuf` form,
            // which is actually returned from tested function
            let mut exp_paths: Vec<PathBuf> = Vec::new();
            for file in exp_files_str {
                let mut path = PathBuf::new();
                path.push(file);
                exp_paths.push(path);
            }

            // Match each path in the result to one (and only one) of the
            // expected paths, removing matched paths from expected list
            for path in result {
                match exp_paths.iter().position(|ep| *ep == path) {
                    Some(i) => exp_paths.remove(i),
                    None => {
                        panic!("File '{}' not found in expected files.", path.display(),);
                    }
                };
            }

            // No paths should remain unmatched in expected list
            assert!(
                exp_paths.is_empty(),
                "Not all paths in expected paths list were matched:\n{:#?}\n",
                exp_paths.iter().map(|ep| ep.display()).collect::<Vec<_>>(),
            );
        }
    }
}
