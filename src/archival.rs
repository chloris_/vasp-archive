//! Archival of VASP calculation directories into a file.
//!
//! This module relies on module [crate::dir_analysis] to recognize calculation
//! directories and obtain lists of files to archive.

use crate::argument_parsing;
use crate::dir_analysis;
use flate2::write::GzEncoder;
use flate2::Compression;
use std::fs::File;
use std::io::{self, prelude::*};
use std::path::{Path, PathBuf};
use tar::Builder;

//
// Constants
//

/// Compression level for Gzip compression.
const COMPRESSION_LEVEL: u32 = 4;

//
// Functions
//

/// Appends a VASP calculation directory `dir` to `archive`. The identity of
/// the directory as a calcualtion directory is not checked.
///
/// Relies on functions in [crate::dir_analysis] to get the list of files
/// to archive.
fn append_directory<P, W>(dir: P, archive: &mut Builder<W>) -> io::Result<()>
where
    P: AsRef<Path>,
    W: Write,
{
    let verbose = argument_parsing::get_verbose();
    let dir = dir.as_ref();

    // Retrieve calculation directory name, that will serve as path in the
    // archive, from given path `dir`
    let dir_name = match dir.file_name() {
        Some(name) => name.to_string_lossy().into_owned(),
        None => {
            return Err(io::Error::new(
                io::ErrorKind::Other,
                format!(
                    "Could not retrieve calculation directory name from '{}'.",
                    dir.display(),
                ),
            ));
        }
    };

    if verbose {
        println!(
            ":: Archiving directory '{}' as '{}'",
            dir.display(),
            dir_name,
        );
    }

    let files = dir_analysis::get_files_to_archive(&dir)?;

    // Add all files to archive in the calculation directory to the archive
    let mut is_first = true;
    for file_path in files {
        // Construct path to the source file
        let mut fs_path = PathBuf::new();
        fs_path.push(&dir);
        fs_path.push(&file_path);

        // Construct desired path of the file in the archive
        let mut archive_path = PathBuf::new();
        archive_path.push(&dir_name);
        archive_path.push(&file_path);

        // In verbose mode print archived file names in a single line
        if verbose {
            if is_first {
                is_first = false;
            } else {
                print!(", ");
            }
            print!("'{}'", archive_path.display());
            let _result = io::stdout().flush();
        }

        // Add the file to the archive
        let mut file = File::open(fs_path)?;
        archive.append_file(archive_path, &mut file)?;
    }
    // Terminate the single line printed into
    if verbose {
        println!("");
    }

    return Ok(());
}

/// Appends all VASP calculation subdirectories of directory `dir` to
/// `archive`. Non-calculation subdirectories are skipped and the archival
/// is non-recursive (subdirectories of subdirectories are not checked).
fn append_subdirectories<P, W>(dir: P, archive: &mut Builder<W>) -> io::Result<()>
where
    P: AsRef<Path>,
    W: Write,
{
    let verbose = argument_parsing::get_verbose();
    let dir = dir.as_ref();

    let calc_dirs = dir_analysis::get_calculation_dirs(&dir)?;

    if verbose {
        println!(
            "{} calculation directories found in '{}'.",
            calc_dirs.len(),
            dir.display(),
        );
    }

    for calc_dir in calc_dirs {
        if verbose {
            println!("Archiving subdirectory '{}'.", calc_dir.display(),);
        }
        append_directory(calc_dir, archive)?;
    }

    return Ok(());
}

/// Archives all calculation subdirectories of directory `dir` to a
/// Gzipped TAR archive at `path`.
pub fn archive_subdirs_to_file<P: AsRef<Path>>(dir: P, path: P) -> io::Result<()> {
    let verbose = argument_parsing::get_verbose();
    let dir = dir.as_ref();
    let path = path.as_ref();

    if verbose {
        println!(
            "Archiving from directory '{}' to file '{}̈́'.",
            dir.display(),
            path.display(),
        );
    }

    let archive_file = File::create(&path)?;
    if verbose {
        println!("Opened archive file '{}̈́'.", path.display(),);
    }
    let encoder = GzEncoder::new(archive_file, Compression::new(COMPRESSION_LEVEL));

    let mut archive = Builder::new(encoder);
    append_subdirectories(dir, &mut archive)?;

    let encoder = archive.into_inner()?;
    if verbose {
        println!("Archive written.");
    }

    let _archive_file = encoder.finish()?;
    if verbose {
        println!("Encoder finished.");
    }

    return Ok(());
}

/// Archives all calculation subdirectories `dirs` to a Gzipped TAR archive
/// at `path`.
pub fn archive_dir_list_to_file<P: AsRef<Path>>(dirs: &Vec<P>, path: &P) -> io::Result<()> {
    let verbose = argument_parsing::get_verbose();
    let path = path.as_ref();

    if verbose {
        println!("Archiving from directories to file '{}̈́'.", path.display(),);
    }

    let archive_file = File::create(&path)?;
    if verbose {
        println!("Opened archive file '{}̈́'.", path.display(),);
    }
    let encoder = GzEncoder::new(archive_file, Compression::new(COMPRESSION_LEVEL));

    let mut archive = Builder::new(encoder);
    for dir in dirs {
        if verbose {
            println!("Archiving directory '{}'.", dir.as_ref().display());
        }
        append_directory(dir, &mut archive)?;
    }

    let encoder = archive.into_inner()?;
    if verbose {
        println!("Archive written.");
    }

    let _archive_file = encoder.finish()?;
    if verbose {
        println!("Encoder finished.");
    }

    return Ok(());
}
