//! Parsing of command line arguments and sharing the results.

use clap::{crate_description, crate_name, crate_version};
use clap::{App, Arg};
use std::cell::RefCell;

//
// Global variables
//

thread_local! {
    /// A flag indicating whether the program should print its output
    /// verbosely.
    static VERBOSE: RefCell<bool> = RefCell::new(false);
}

//
// Structs
//

/// Container for parsed command line arguments.
pub struct CommandLineArgs {
    pub verbose: bool,
    pub output_archive: String,
    pub dirs_to_archive: Vec<String>,
}

///
/// Functions
///

/// Parses command line arguments and returns them as [CommandLineArgs].
pub fn parse_arguments() -> CommandLineArgs {
    // Parse the arguments
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Makes the program print output more liberally"),
        )
        .arg(
            Arg::with_name("output_archive")
                .short("o")
                .long("output")
                .help("Path to the output archive")
                .value_name("OUTPUT_ARCHIVE")
                .default_value("pack.tar.gz"),
        )
        .arg(
            Arg::with_name("dirs_to_archive")
                .takes_value(true)
                .multiple(true)
                .value_name("DIRS_TO_ARCHIVE")
                .help(
                    "Paths to calculation directories to archive. If none \
                    are provided, looks for calculation directories in the \
                    current working directory and archives them.",
                ),
        )
        .get_matches();

    // Store the `verbose` flag for global access using [get_verbose]
    VERBOSE.with(|v| *v.borrow_mut() = matches.is_present("verbose"));

    // Return packed command line args
    return CommandLineArgs {
        verbose: matches.is_present("verbose"),
        output_archive: matches.value_of("output_archive").unwrap().to_string(),
        dirs_to_archive: match matches.values_of("dirs_to_archive") {
            Some(iter) => iter.map(|s| s.to_string()).collect(),
            None => Vec::new(),
        },
    };
}

/// Returns the state of the `verbose` flag.
pub fn get_verbose() -> bool {
    return VERBOSE.with(|v| *v.borrow());
}
