# vasp-archive

A simple program that archives subdirectories with VASP calculations for easy
transfer between computers.

The archival format used is TAR with Gzip compression.


## Building

`vasp-archive` is written in Rust and should therefore be compiled using Cargo:

```sh
cargo build --release
```

The executable should appear in `target/release`.


## Usage

There are two main forms of usage:

- archiving all calculation subdirectories in the current working directory
- archiving directories explicitly passed via command line arguments

Usage information can be displayed by passing `-h` or `--help` to the
program.

All subdirectories, that are recognized as VASP calculation subdirectories by
names of contained files, can be archived as a default action of the program:

```sh
vasp-archive
```

If desired, path to the output archive can be specified (the default value
is mentioned in the usage information):

```sh
vasp-archive -o archives/my_archive.tar.gz
```

Archival of a custom list of calculation directories is performed by passing
their paths as command line arguments:

```sh
vasp-archive directory_1 calculations/directory_2 folder/directory_3
```
